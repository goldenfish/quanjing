/*!
 * 切图全景 QietuScener v1.0
 *
 * Copyright 2014 quanjing.qietu.com, Inc
 * Licensed under the Apache License v2.0
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 */
 
 
 $(function() {
    /*$(".panel").css({"height":$(window).height()});
	$.scrollify({
		section:".panel",
		before:function(i){
			if(i==0){
				//$(i).preventDefault();	
			}
		},
		after:function(i){
			$('.QietuScenerNav li').removeClass('current');
			$('.QietuScenerNav li').eq(i).addClass('current');
			}
	});
	

	$(".QietuScenerNav li").click(function(e) {
		e.preventDefault();
		$.scrollify("move",$(this).find('a').attr("href"));
	});*/
	
	$('.QietuScenerPage').css('height',$(window).height());
	
	$(window).resize(function(){
		$('.QietuScenerPage').css('height',$(window).height());					  
	})
	
	/*找到显示的那页并且显示*/
	$($('.QietuScenerNav .selected a').attr('href')).addClass('active').show();
	
	$('html,body').animate({'scrollTop':0});
	
	var __animating = false;
	var scrollpage = function(direc,next_page){
			
			if( __animating ){
					return true;
				}
				
			direc = direc || 'DOWN';
			/*向下*/
			
			curr_nav_li = $('.QietuScenerNav .selected');
			
			
				if(direc== 'DOWN'){
					//console.log($(this).scrollTop());
					
					
					next_nav_li = curr_nav_li.next();
					
				}
				/*向上*/
				else if(direc== 'UP'){
				
					next_nav_li = $('.QietuScenerNav .selected').prev();
						
				}
			 
			
			
			if(next_nav_li.size()<1){
				return false;	
			}
			curr_page = $(curr_nav_li.find('a').attr('href'));
			
			if( !next_page || next_page.length == 0 ){
				next_page = $(next_nav_li.find('a').attr('href'));	
			}
			
			
			/*当前页添加active样式*/
			//$('.QietuScenerPage').removeClass('active');
			
			 var CSS_ANIM = 'body-section-animing';
					CSS_CURR = 'active',
			CSS_Z_S = 'anim-zoom-s',
					CSS_Z_M = 'anim-zoom-m',
					CSS_Z_B = 'anim-zoom-b',
					CSS_Z_PLAY = 'anim-zoom-play',
					ANIM_TIME = 450;
					
			if(direc == 'UP'){					
					_cur_zoom_start = CSS_Z_M;
					_cur_zoom_end = CSS_Z_B;
					_go_zoom_start = CSS_Z_S;
					_go_zoom_end = CSS_Z_M;

				}else if(direc == 'DOWN'){
					_cur_zoom_start = CSS_Z_M;
					_cur_zoom_end = CSS_Z_S;
					_go_zoom_start = CSS_Z_B;
					_go_zoom_end = CSS_Z_M;

				}
				
			
	
			__animating = true;
			
			next_page_inner = next_page.find('.QietuScenerInner');
			next_page.addClass(CSS_ANIM).fadeOut(0);
			
			var curr_page_inner = curr_page.find('.QietuScenerInner');
			curr_page_inner.addClass('_cur_zoom_start').addClass(CSS_Z_PLAY);
			
			setTimeout(function(){
					curr_page_inner.removeClass(_cur_zoom_start).addClass(_cur_zoom_end)
				}, 10);

				curr_page.fadeOut(ANIM_TIME, function(){
					curr_page_inner.removeClass( [CSS_Z_S, CSS_Z_M, CSS_Z_B, CSS_Z_PLAY].join(' ') );
				});				

				setTimeout(function(){
					next_page_inner.addClass(_go_zoom_start).addClass(CSS_Z_PLAY);
					setTimeout(function(){
						next_page_inner.removeClass(_go_zoom_start).addClass(_go_zoom_end)
					}, 10);

					next_page.fadeIn(ANIM_TIME, function() {
						curr_page.removeClass(CSS_CURR);
						next_page.removeClass(CSS_ANIM).addClass(CSS_CURR);

						next_page_inner.removeClass( [CSS_Z_S, CSS_Z_M, CSS_Z_B, CSS_Z_PLAY].join(' ') );

						__animating = false;
					});
				}, ANIM_TIME/3);
			
			
	
										
				/*状态按钮切换*/						
				$('.QietuScenerNav li').removeClass('selected');
				next_nav_li.addClass('selected');
				
				if(!next_nav_li.hasClass('hide')){
					$('.QietuScenerNav li').removeClass('QietuScenerNavCurr');
					next_nav_li.addClass('QietuScenerNavCurr');
				}
				
			//});
	}
	
	var i = !1; 
	$('body').mousewheel(function(event, delta) {
			
				var dir = delta > 0 ? 'UP' : 'DOWN';
						
						if (!$('html,body').is(":animated") && !i) {
							i = !0,
							setTimeout(function() {
								i = !1
							},
							'normal');
							
							scrollpage(dir);
							
						}
						
						
      });
	
	
	$('.QietuScenerNav li').click(function(){
											
		//$('.QietuScenerNav li').removeClass('QietuScenerNavCurr').eq($(this).index()).addClass('QietuScenerNavCurr');
		//$('.QietuScenerNav li').removeClass('selected').eq($(this).index()).addClass('selected');		
		//scrollpage('DOWN',$($(this).find('a').attr('href'))  );								  
		
		 
		
		
	})
	
});

